/*global getCaretCoordinates*/

import { extend } from 'flarum/extend';


import ComposerBody from 'flarum/components/ComposerBody';
import highlight from 'flarum/helpers/highlight';
import avatar from 'flarum/helpers/avatar';
import { truncate } from 'flarum/utils/string';

import AutocompleteDropdown from 'mentions/binh/components/AutocompleteDropdown';

export default function addComposerAutocomplete() {
  extend(ComposerBody.prototype, 'config', function(original, isInitialized) {
    if (isInitialized) return;

    const composer = this;
    console.log(app.store);
    const $container = $('<div class="ComposerBody-mentionsDropdownContainer"></div>');
    const dropdown = new AutocompleteDropdown({items: []});
    const $textarea = this.$('textarea');
    const searched = [];
    let mentionStart;
    let typed;
    let searchTimeout;

    const applySuggestion = function(replacement) {
      const insert = replacement + ' ';

      const content = composer.content();
      composer.editor.setValue(content.substring(0, mentionStart - 1) + insert + content.substr($textarea[0].selectionStart));

      const index = mentionStart - 1 + insert.length;
      composer.editor.setSelectionRange(index, index);

      dropdown.hide();
    };
    $textarea
      .after($container)
      .on('keydown', dropdown.navigate.bind(dropdown))
      .on('click keyup', function(e) {
        // Up, down, enter, tab, escape, left, right.
        if ([9, 13, 27, 40, 38, 37, 39].indexOf(e.which) !== -1) return;

        const cursor = this.selectionStart;

        if (this.selectionEnd - cursor > 0) return;

        // Search backwards from the cursor for an '~' symbol, without any
        // intervening whitespace. If we find one, we will want to show the
        // autocomplete dropdown!
        const value = this.value;
        mentionStart = 0;
        for (let i = cursor - 1; i >= 0; i--) {
          const character = value.substr(i, 1);
          if (/\s/.test(character)) break;
          if (character === '~') {
            mentionStart = i + 1;
            break;
          }
        }

        dropdown.hide();
        dropdown.active = false;

        if (mentionStart) {
          typed = value.substring(mentionStart, cursor).toLowerCase();

          const makeSuggestion = function(discussion, user, replacement, content, className = '') {
            if (typed) {
              content = highlight(content, typed);
            }

            return (
              <button className={'PostPreview ' + className}
                onclick={() => applySuggestion(replacement)}
                onmouseenter={function() {
                  dropdown.setIndex($(this).parent().index());
                }}>
                <span className="PostPreview-content">
                 {avatar(user)}
                 {content}
                </span>
              </button>
            );
          };

          const buildSuggestions = () => {
            const suggestions = [];
            const composerPost = composer.props.post;
            const discuss = (composerPost && composerPost.discussion()) || composer.props.discussion;
            const discussionId = discuss.id();
            if (typed) {
              app.store.all('discussions').forEach(discussion => {
              const typedLower = typed.toLowerCase();
              if (discussion.title().toLowerCase().indexOf(typedLower) === -1 || discussion.id() == discussionId) return;
              var slug = getSlug(discussion.title(), {
				        lang: 'vn'
				    });
				    	if(discussion.data.relationships.startUser){
				    	var userId = discussion.data.relationships.startUser.data.id;
				    	var user = app.store.getById("users", userId);
                suggestions.push(
                  makeSuggestion(discussion, user, '[URL=/d/' +  discussion.id() + '-' + userId + ']' + discussion.title() + '[/URL]'  , discussion.title(), 'MentionsDropdown-user')
                );
               }
              });
            }

            if (suggestions.length) {
              dropdown.props.items = suggestions;
              m.render($container[0], dropdown.render());

              dropdown.show();
              const coordinates = getCaretCoordinates(this, mentionStart);
              const width = dropdown.$().outerWidth();
              const height = dropdown.$().outerHeight();
              const parent = dropdown.$().offsetParent();
              let left = coordinates.left;
              let top = coordinates.top + 15;
              if (top + height > parent.height()) {
                top = coordinates.top - height - 15;
              }
              if (left + width > parent.width()) {
                left = parent.width() - width;
              }
              dropdown.show(left, top);
            }
          };

          buildSuggestions();

          dropdown.setIndex(0);
          dropdown.$().scrollTop(0);
          dropdown.active = true;

          clearTimeout(searchTimeout);
          if (typed) {
            searchTimeout = setTimeout(function() {
              const typedLower = typed.toLowerCase();
              if (searched.indexOf(typedLower) === -1) {
                app.store.find('discussions', {q: typed, page: {limit: 10}}).then(() => {
                  if (dropdown.active) buildSuggestions();
                });
                searched.push(typedLower);
              }
            }, 250);
          }
        }
      });
  });
}



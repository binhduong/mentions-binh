/* jshint browser: true */

(function () {

// The properties that we copy into a mirrored div.
// Note that some browsers, such as Firefox,
// do not concatenate properties, i.e. padding-top, bottom etc. -> padding,
// so we have to do every single property specifically.
var properties = [
  'direction',  // RTL support
  'boxSizing',
  'width',  // on Chrome and IE, exclude the scrollbar, so the mirror div wraps exactly as the textarea does
  'height',
  'overflowX',
  'overflowY',  // copy the scrollbar for IE

  'borderTopWidth',
  'borderRightWidth',
  'borderBottomWidth',
  'borderLeftWidth',
  'borderStyle',

  'paddingTop',
  'paddingRight',
  'paddingBottom',
  'paddingLeft',

  // https://developer.mozilla.org/en-US/docs/Web/CSS/font
  'fontStyle',
  'fontVariant',
  'fontWeight',
  'fontStretch',
  'fontSize',
  'fontSizeAdjust',
  'lineHeight',
  'fontFamily',

  'textAlign',
  'textTransform',
  'textIndent',
  'textDecoration',  // might not make a difference, but better be safe

  'letterSpacing',
  'wordSpacing',

  'tabSize',
  'MozTabSize'

];

var isFirefox = window.mozInnerScreenX != null;

function getCaretCoordinates(element, position) {
  // mirrored div
  var div = document.createElement('div');
  div.id = 'input-textarea-caret-position-mirror-div';
  document.body.appendChild(div);

  var style = div.style;
  var computed = window.getComputedStyle? getComputedStyle(element) : element.currentStyle;  // currentStyle for IE < 9

  // default textarea styles
  style.whiteSpace = 'pre-wrap';
  if (element.nodeName !== 'INPUT')
    style.wordWrap = 'break-word';  // only for textarea-s

  // position off-screen
  style.position = 'absolute';  // required to return coordinates properly
  style.visibility = 'hidden';  // not 'display: none' because we want rendering

  // transfer the element's properties to the div
  properties.forEach(function (prop) {
    style[prop] = computed[prop];
  });

  if (isFirefox) {
    // Firefox lies about the overflow property for textareas: https://bugzilla.mozilla.org/show_bug.cgi?id=984275
    if (element.scrollHeight > parseInt(computed.height))
      style.overflowY = 'scroll';
  } else {
    style.overflow = 'hidden';  // for Chrome to not render a scrollbar; IE keeps overflowY = 'scroll'
  }

  div.textContent = element.value.substring(0, position);
  // the second special handling for input type="text" vs textarea: spaces need to be replaced with non-breaking spaces - http://stackoverflow.com/a/13402035/1269037
  if (element.nodeName === 'INPUT')
    div.textContent = div.textContent.replace(/\s/g, "\u00a0");

  var span = document.createElement('span');
  // Wrapping must be replicated *exactly*, including when a long word gets
  // onto the next line, with whitespace at the end of the line before (#7).
  // The  *only* reliable way to do that is to copy the *entire* rest of the
  // textarea's content into the <span> created at the caret position.
  // for inputs, just '.' would be enough, but why bother?
  span.textContent = element.value.substring(position) || '.';  // || because a completely empty faux span doesn't render at all
  div.appendChild(span);

  var coordinates = {
    top: span.offsetTop + parseInt(computed['borderTopWidth']),
    left: span.offsetLeft + parseInt(computed['borderLeftWidth'])
  };

  document.body.removeChild(div);

  return coordinates;
}

if (typeof module != "undefined" && typeof module.exports != "undefined") {
  module.exports = getCaretCoordinates;
} else {
  window.getCaretCoordinates = getCaretCoordinates;
}

}());
;
/**
 * speakingurl
 * @version v7.0.0
 * @link http://pid.github.io/speakingurl/
 * @license BSD
 * @author Sascha Droste
 */!function(e,a){"use strict";var n=function(e,a){var n,t,u,l,s,r,m,c,h,d,g,k,f,y,p,A="-",b=[";","?",":","@","&","=","+","$",",","/"],z=[";","?",":","@","&","=","+","$",","],E=[".","!","~","*","'","(",")"],j="",S="",O=!0,v={},w={"À":"A","Á":"A","Â":"A","Ã":"A","Ä":"Ae","Å":"A","Æ":"AE","Ç":"C","È":"E","É":"E","Ê":"E","Ë":"E","Ì":"I","Í":"I","Î":"I","Ï":"I","Ð":"D","Ñ":"N","Ò":"O","Ó":"O","Ô":"O","Õ":"O","Ö":"Oe","Ő":"O","Ø":"O","Ù":"U","Ú":"U","Û":"U","Ü":"Ue","Ű":"U","Ý":"Y","Þ":"TH","ß":"ss","à":"a","á":"a","â":"a","ã":"a","ä":"ae","å":"a","æ":"ae","ç":"c","è":"e","é":"e","ê":"e","ë":"e","ì":"i","í":"i","î":"i","ï":"i","ð":"d","ñ":"n","ò":"o","ó":"o","ô":"o","õ":"o","ö":"oe","ő":"o","ø":"o","ù":"u","ú":"u","û":"u","ü":"ue","ű":"u","ý":"y","þ":"th","ÿ":"y","ẞ":"SS","ا":"a","أ":"a","إ":"i","آ":"aa","ؤ":"u","ئ":"e","ء":"a","ب":"b","ت":"t","ث":"th","ج":"j","ح":"h","خ":"kh","د":"d","ذ":"th","ر":"r","ز":"z","س":"s","ش":"sh","ص":"s","ض":"dh","ط":"t","ظ":"z","ع":"a","غ":"gh","ف":"f","ق":"q","ك":"k","ل":"l","م":"m","ن":"n","ه":"h","و":"w","ي":"y","ى":"a","ة":"h","ﻻ":"la","ﻷ":"laa","ﻹ":"lai","ﻵ":"laa","َ":"a","ً":"an","ِ":"e","ٍ":"en","ُ":"u","ٌ":"on","ْ":"","٠":"0","١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9","က":"k","ခ":"kh","ဂ":"g","ဃ":"ga","င":"ng","စ":"s","ဆ":"sa","ဇ":"z","စျ":"za","ည":"ny","ဋ":"t","ဌ":"ta","ဍ":"d","ဎ":"da","ဏ":"na","တ":"t","ထ":"ta","ဒ":"d","ဓ":"da","န":"n","ပ":"p","ဖ":"pa","ဗ":"b","ဘ":"ba","မ":"m","ယ":"y","ရ":"ya","လ":"l","ဝ":"w","သ":"th","ဟ":"h","ဠ":"la","အ":"a","ြ":"y","ျ":"ya","ွ":"w","ြွ":"yw","ျွ":"ywa","ှ":"h","ဧ":"e","၏":"-e","ဣ":"i","ဤ":"-i","ဉ":"u","ဦ":"-u","ဩ":"aw","သြော":"aw","ဪ":"aw","၀":"0","၁":"1","၂":"2","၃":"3","၄":"4","၅":"5","၆":"6","၇":"7","၈":"8","၉":"9","္":"","့":"","း":"","č":"c","ď":"d","ě":"e","ň":"n","ř":"r","š":"s","ť":"t","ů":"u","ž":"z","Č":"C","Ď":"D","Ě":"E","Ň":"N","Ř":"R","Š":"S","Ť":"T","Ů":"U","Ž":"Z","ހ":"h","ށ":"sh","ނ":"n","ރ":"r","ބ":"b","ޅ":"lh","ކ":"k","އ":"a","ވ":"v","މ":"m","ފ":"f","ދ":"dh","ތ":"th","ލ":"l","ގ":"g","ޏ":"gn","ސ":"s","ޑ":"d","ޒ":"z","ޓ":"t","ޔ":"y","ޕ":"p","ޖ":"j","ޗ":"ch","ޘ":"tt","ޙ":"hh","ޚ":"kh","ޛ":"th","ޜ":"z","ޝ":"sh","ޞ":"s","ޟ":"d","ޠ":"t","ޡ":"z","ޢ":"a","ޣ":"gh","ޤ":"q","ޥ":"w","ަ":"a","ާ":"aa","ި":"i","ީ":"ee","ު":"u","ޫ":"oo","ެ":"e","ޭ":"ey","ޮ":"o","ޯ":"oa","ް":"","α":"a","β":"v","γ":"g","δ":"d","ε":"e","ζ":"z","η":"i","θ":"th","ι":"i","κ":"k","λ":"l","μ":"m","ν":"n","ξ":"ks","ο":"o","π":"p","ρ":"r","σ":"s","τ":"t","υ":"y","φ":"f","χ":"x","ψ":"ps","ω":"o","ά":"a","έ":"e","ί":"i","ό":"o","ύ":"y","ή":"i","ώ":"o","ς":"s","ϊ":"i","ΰ":"y","ϋ":"y","ΐ":"i","Α":"A","Β":"B","Γ":"G","Δ":"D","Ε":"E","Ζ":"Z","Η":"I","Θ":"TH","Ι":"I","Κ":"K","Λ":"L","Μ":"M","Ν":"N","Ξ":"KS","Ο":"O","Π":"P","Ρ":"R","Σ":"S","Τ":"T","Υ":"Y","Φ":"F","Χ":"X","Ψ":"PS","Ω":"W","Ά":"A","Έ":"E","Ί":"I","Ό":"O","Ύ":"Y","Ή":"I","Ώ":"O","Ϊ":"I","Ϋ":"Y","ā":"a","ē":"e","ģ":"g","ī":"i","ķ":"k","ļ":"l","ņ":"n","ū":"u","Ā":"A","Ē":"E","Ģ":"G","Ī":"I","Ķ":"k","Ļ":"L","Ņ":"N","Ū":"U","Ќ":"Kj","ќ":"kj","Љ":"Lj","љ":"lj","Њ":"Nj","њ":"nj","Тс":"Ts","тс":"ts","ą":"a","ć":"c","ę":"e","ł":"l","ń":"n","ś":"s","ź":"z","ż":"z","Ą":"A","Ć":"C","Ę":"E","Ł":"L","Ń":"N","Ś":"S","Ź":"Z","Ż":"Z","Є":"Ye","І":"I","Ї":"Yi","Ґ":"G","є":"ye","і":"i","ї":"yi","ґ":"g","ă":"a","Ă":"A","ș":"s","Ș":"S","ț":"t","Ț":"T","ţ":"t","Ţ":"T","а":"a","б":"b","в":"v","г":"g","д":"d","е":"e","ё":"yo","ж":"zh","з":"z","и":"i","й":"j","к":"k","л":"l","м":"m","н":"n","о":"o","п":"p","р":"r","с":"s","т":"t","у":"u","ф":"f","х":"h","ц":"c","ч":"ch","ш":"sh","щ":"sh","ъ":"","ы":"y","ь":"","э":"e","ю":"yu","я":"ya","А":"A","Б":"B","В":"V","Г":"G","Д":"D","Е":"E","Ё":"Yo","Ж":"Zh","З":"Z","И":"I","Й":"J","К":"K","Л":"L","М":"M","Н":"N","О":"O","П":"P","Р":"R","С":"S","Т":"T","У":"U","Ф":"F","Х":"H","Ц":"C","Ч":"Ch","Ш":"Sh","Щ":"Sh","Ъ":"","Ы":"Y","Ь":"","Э":"E","Ю":"Yu","Я":"Ya","ђ":"dj","ј":"j","ћ":"c","џ":"dz","Ђ":"Dj","Ј":"j","Ћ":"C","Џ":"Dz","ľ":"l","ĺ":"l","ŕ":"r","Ľ":"L","Ĺ":"L","Ŕ":"R","ş":"s","Ş":"S","ı":"i","İ":"I","ğ":"g","Ğ":"G","ả":"a","Ả":"A","ẳ":"a","Ẳ":"A","ẩ":"a","Ẩ":"A","đ":"d","Đ":"D","ẹ":"e","Ẹ":"E","ẽ":"e","Ẽ":"E","ẻ":"e","Ẻ":"E","ế":"e","Ế":"E","ề":"e","Ề":"E","ệ":"e","Ệ":"E","ễ":"e","Ễ":"E","ể":"e","Ể":"E","ọ":"o","Ọ":"o","ố":"o","Ố":"O","ồ":"o","Ồ":"O","ổ":"o","Ổ":"O","ộ":"o","Ộ":"O","ỗ":"o","Ỗ":"O","ơ":"o","Ơ":"O","ớ":"o","Ớ":"O","ờ":"o","Ờ":"O","ợ":"o","Ợ":"O","ỡ":"o","Ỡ":"O","Ở":"o","ở":"o","ị":"i","Ị":"I","ĩ":"i","Ĩ":"I","ỉ":"i","Ỉ":"i","ủ":"u","Ủ":"U","ụ":"u","Ụ":"U","ũ":"u","Ũ":"U","ư":"u","Ư":"U","ứ":"u","Ứ":"U","ừ":"u","Ừ":"U","ự":"u","Ự":"U","ữ":"u","Ữ":"U","ử":"u","Ử":"ư","ỷ":"y","Ỷ":"y","ỳ":"y","Ỳ":"Y","ỵ":"y","Ỵ":"Y","ỹ":"y","Ỹ":"Y","ạ":"a","Ạ":"A","ấ":"a","Ấ":"A","ầ":"a","Ầ":"A","ậ":"a","Ậ":"A","ẫ":"a","Ẫ":"A","ắ":"a","Ắ":"A","ằ":"a","Ằ":"A","ặ":"a","Ặ":"A","ẵ":"a","Ẵ":"A","“":'"',"”":'"',"‘":"'","’":"'","∂":"d","ƒ":"f","™":"(TM)","©":"(C)","œ":"oe","Œ":"OE","®":"(R)","†":"+","℠":"(SM)","…":"...","˚":"o","º":"o","ª":"a","•":"*","၊":",","။":".",$:"USD","€":"EUR","₢":"BRN","₣":"FRF","£":"GBP","₤":"ITL","₦":"NGN","₧":"ESP","₩":"KRW","₪":"ILS","₫":"VND","₭":"LAK","₮":"MNT","₯":"GRD","₱":"ARS","₲":"PYG","₳":"ARA","₴":"UAH","₵":"GHS","¢":"cent","¥":"CNY","元":"CNY","円":"YEN","﷼":"IRR","₠":"EWE","฿":"THB","₨":"INR","₹":"INR","₰":"PF"},U=["်","ް"],C={"ာ":"a","ါ":"a","ေ":"e","ဲ":"e","ိ":"i","ီ":"i","ို":"o","ု":"u","ူ":"u","ေါင်":"aung","ော":"aw","ော်":"aw","ေါ":"aw","ေါ်":"aw","်":"်","က်":"et","ိုက်":"aik","ောက်":"auk","င်":"in","ိုင်":"aing","ောင်":"aung","စ်":"it","ည်":"i","တ်":"at","ိတ်":"eik","ုတ်":"ok","ွတ်":"ut","ေတ်":"it","ဒ်":"d","ိုဒ်":"ok","ုဒ်":"ait","န်":"an","ာန်":"an","ိန်":"ein","ုန်":"on","ွန်":"un","ပ်":"at","ိပ်":"eik","ုပ်":"ok","ွပ်":"ut","န်ုပ်":"nub","မ်":"an","ိမ်":"ein","ုမ်":"on","ွမ်":"un","ယ်":"e","ိုလ်":"ol","ဉ်":"in","ံ":"an","ိံ":"ein","ုံ":"on","ައް":"ah","ަށް":"ah"},I={en:{},az:{"ç":"c","ə":"e","ğ":"g","ı":"i","ö":"o","ş":"s","ü":"u","Ç":"C","Ə":"E","Ğ":"G","İ":"I","Ö":"O","Ş":"S","Ü":"U"},cs:{"č":"c","ď":"d","ě":"e","ň":"n","ř":"r","š":"s","ť":"t","ů":"u","ž":"z","Č":"C","Ď":"D","Ě":"E","Ň":"N","Ř":"R","Š":"S","Ť":"T","Ů":"U","Ž":"Z"},fi:{"ä":"a","Ä":"A","ö":"o","Ö":"O"},hu:{"ä":"a","Ä":"A","ö":"o","Ö":"O","ü":"u","Ü":"U","ű":"u","Ű":"U"},lt:{"ą":"a","č":"c","ę":"e","ė":"e","į":"i","š":"s","ų":"u","ū":"u","ž":"z","Ą":"A","Č":"C","Ę":"E","Ė":"E","Į":"I","Š":"S","Ų":"U","Ū":"U"},lv:{"ā":"a","č":"c","ē":"e","ģ":"g","ī":"i","ķ":"k","ļ":"l","ņ":"n","š":"s","ū":"u","ž":"z","Ā":"A","Č":"C","Ē":"E","Ģ":"G","Ī":"i","Ķ":"k","Ļ":"L","Ņ":"N","Š":"S","Ū":"u","Ž":"Z"},pl:{"ą":"a","ć":"c","ę":"e","ł":"l","ń":"n","ó":"o","ś":"s","ź":"z","ż":"z","Ą":"A","Ć":"C","Ę":"e","Ł":"L","Ń":"N","Ó":"O","Ś":"S","Ź":"Z","Ż":"Z"},sk:{"ä":"a","Ä":"A"},sr:{"љ":"lj","њ":"nj","Љ":"Lj","Њ":"Nj","đ":"dj","Đ":"Dj"},tr:{"Ü":"U","Ö":"O","ü":"u","ö":"o"}},N={ar:{"∆":"delta","∞":"la-nihaya","♥":"hob","&":"wa","|":"aw","<":"aqal-men",">":"akbar-men","∑":"majmou","¤":"omla"},az:{},ca:{"∆":"delta","∞":"infinit","♥":"amor","&":"i","|":"o","<":"menys que",">":"mes que","∑":"suma dels","¤":"moneda"},cz:{"∆":"delta","∞":"nekonecno","♥":"laska","&":"a","|":"nebo","<":"mene jako",">":"vice jako","∑":"soucet","¤":"mena"},de:{"∆":"delta","∞":"unendlich","♥":"Liebe","&":"und","|":"oder","<":"kleiner als",">":"groesser als","∑":"Summe von","¤":"Waehrung"},dv:{"∆":"delta","∞":"kolunulaa","♥":"loabi","&":"aai","|":"noonee","<":"ah vure kuda",">":"ah vure bodu","∑":"jumula","¤":"faisaa"},en:{"∆":"delta","∞":"infinity","♥":"love","&":"and","|":"or","<":"less than",">":"greater than","∑":"sum","¤":"currency"},es:{"∆":"delta","∞":"infinito","♥":"amor","&":"y","|":"u","<":"menos que",">":"mas que","∑":"suma de los","¤":"moneda"},fr:{"∆":"delta","∞":"infiniment","♥":"Amour","&":"et","|":"ou","<":"moins que",">":"superieure a","∑":"somme des","¤":"monnaie"},gr:{},hu:{"∆":"delta","∞":"vegtelen","♥":"szerelem","&":"es","|":"vagy","<":"kisebb mint",">":"nagyobb mint","∑":"szumma","¤":"penznem"},it:{"∆":"delta","∞":"infinito","♥":"amore","&":"e","|":"o","<":"minore di",">":"maggiore di","∑":"somma","¤":"moneta"},lt:{},lv:{"∆":"delta","∞":"bezgaliba","♥":"milestiba","&":"un","|":"vai","<":"mazak neka",">":"lielaks neka","∑":"summa","¤":"valuta"},my:{"∆":"kwahkhyaet","∞":"asaonasme","♥":"akhyait","&":"nhin","|":"tho","<":"ngethaw",">":"kyithaw","∑":"paungld","¤":"ngwekye"},mk:{},nl:{"∆":"delta","∞":"oneindig","♥":"liefde","&":"en","|":"of","<":"kleiner dan",">":"groter dan","∑":"som","¤":"valuta"},pl:{"∆":"delta","∞":"nieskonczonosc","♥":"milosc","&":"i","|":"lub","<":"mniejsze niz",">":"wieksze niz","∑":"suma","¤":"waluta"},pt:{"∆":"delta","∞":"infinito","♥":"amor","&":"e","|":"ou","<":"menor que",">":"maior que","∑":"soma","¤":"moeda"},ro:{"∆":"delta","∞":"infinit","♥":"dragoste","&":"si","|":"sau","<":"mai mic ca",">":"mai mare ca","∑":"suma","¤":"valuta"},ru:{"∆":"delta","∞":"beskonechno","♥":"lubov","&":"i","|":"ili","<":"menshe",">":"bolshe","∑":"summa","¤":"valjuta"},sk:{"∆":"delta","∞":"nekonecno","♥":"laska","&":"a","|":"alebo","<":"menej ako",">":"viac ako","∑":"sucet","¤":"mena"},sr:{},tr:{"∆":"delta","∞":"sonsuzluk","♥":"ask","&":"ve","|":"veya","<":"kucuktur",">":"buyuktur","∑":"toplam","¤":"para birimi"},uk:{"∆":"delta","∞":"bezkinechnist","♥":"lubov","&":"i","|":"abo","<":"menshe",">":"bilshe","∑":"suma","¤":"valjuta"},vn:{"∆":"delta","∞":"vo cuc","♥":"yeu","&":"va","|":"hoac","<":"nho hon",">":"lon hon","∑":"tong","¤":"tien te"}};if("string"!=typeof e)return"";if("string"==typeof a&&(A=a),m=N.en,c=I.en,"object"==typeof a){n=a.maintainCase||!1,v=a.custom&&"object"==typeof a.custom?a.custom:v,u=+a.truncate>1&&a.truncate||!1,l=a.uric||!1,s=a.uricNoSlash||!1,r=a.mark||!1,O=a.symbols===!1||a.lang===!1?!1:!0,A=a.separator||A,l&&(p+=b.join("")),s&&(p+=z.join("")),r&&(p+=E.join("")),m=a.lang&&N[a.lang]&&O?N[a.lang]:O?N.en:{},c=a.lang&&I[a.lang]?I[a.lang]:a.lang===!1||a.lang===!0?{}:I.en,a.titleCase&&"number"==typeof a.titleCase.length&&Array.prototype.toString.call(a.titleCase)?(a.titleCase.forEach(function(e){v[e+""]=e+""}),t=!0):t=!!a.titleCase,a.custom&&"number"==typeof a.custom.length&&Array.prototype.toString.call(a.custom)&&a.custom.forEach(function(e){v[e+""]=e+""}),Object.keys(v).forEach(function(a){var n;n=a.length>1?new RegExp("\\b"+o(a)+"\\b","gi"):new RegExp(o(a),"gi"),e=e.replace(n,v[a])});for(g in v)p+=g}for(p+=A,t&&(e=e.replace(/(\w)(\S*)/g,function(e,a,n){var t=a.toUpperCase()+(null!==n?n:"");return Object.keys(v).indexOf(t.toLowerCase())<0?t:t.toLowerCase()})),p=o(p),e=e.replace(/(^\s+|\s+$)/g,""),f=!1,y=!1,d=0,k=e.length;k>d;d++)g=e[d],i(g,v)?f=!1:c[g]?(g=f&&c[g].match(/[A-Za-z0-9]/)?" "+c[g]:c[g],f=!1):g in w?(k>d+1&&U.indexOf(e[d+1])>=0?(S+=g,g=""):y===!0?(g=C[S]+w[g],S=""):g=f&&w[g].match(/[A-Za-z0-9]/)?" "+w[g]:w[g],f=!1,y=!1):g in C?(S+=g,g="",d===k-1&&(g=C[S]),y=!0):!m[g]||l&&-1!==b.join("").indexOf(g)||s&&-1!==z.join("").indexOf(g)?(y===!0?(g=C[S]+g,S="",y=!1):f&&(/[A-Za-z0-9]/.test(g)||j.substr(-1).match(/A-Za-z0-9]/))&&(g=" "+g),f=!1):(g=f||j.substr(-1).match(/[A-Za-z0-9]/)?A+m[g]:m[g],g+=void 0!==e[d+1]&&e[d+1].match(/[A-Za-z0-9]/)?A:"",f=!0),j+=g.replace(new RegExp("[^\\w\\s"+p+"_-]","g"),A);return j=j.replace(/\s+/g,A).replace(new RegExp("\\"+A+"+","g"),A).replace(new RegExp("(^\\"+A+"+|\\"+A+"+$)","g"),""),u&&j.length>u&&(h=j.charAt(u)===A,j=j.slice(0,u),h||(j=j.slice(0,j.lastIndexOf(A)))),n||t||(j=j.toLowerCase()),j},t=function(e){return function(a){return n(a,e)}},o=function(e){return e.replace(/[-\\^$*+?.()|[\]{}\/]/g,"\\$&")},i=function(e,a){for(var n in a)if(a[n]===e)return!0};if("undefined"!=typeof module&&module.exports)module.exports=n,module.exports.createSlug=t;else if("undefined"!=typeof define&&define.amd)define([],function(){return n});else try{if(e.getSlug||e.createSlug)throw"speakingurl: globals exists /(getSlug|createSlug)/";e.getSlug=n,e.createSlug=t}catch(u){}}(this);;
System.register('mentions/binh/addComposerAutocomplete', ['flarum/extend', 'flarum/components/ComposerBody', 'flarum/helpers/highlight', 'flarum/helpers/avatar', 'flarum/utils/string', 'mentions/binh/components/AutocompleteDropdown'], function (_export) {
  /*global getCaretCoordinates*/

  'use strict';

  var extend, ComposerBody, highlight, avatar, truncate, AutocompleteDropdown;

  _export('default', addComposerAutocomplete);

  function addComposerAutocomplete() {
    extend(ComposerBody.prototype, 'config', function (original, isInitialized) {
      if (isInitialized) return;

      var composer = this;
      console.log(app.store);
      var $container = $('<div class="ComposerBody-mentionsDropdownContainer"></div>');
      var dropdown = new AutocompleteDropdown({ items: [] });
      var $textarea = this.$('textarea');
      var searched = [];
      var mentionStart = undefined;
      var typed = undefined;
      var searchTimeout = undefined;

      var applySuggestion = function applySuggestion(replacement) {
        var insert = replacement + ' ';

        var content = composer.content();
        composer.editor.setValue(content.substring(0, mentionStart - 1) + insert + content.substr($textarea[0].selectionStart));

        var index = mentionStart - 1 + insert.length;
        composer.editor.setSelectionRange(index, index);

        dropdown.hide();
      };
      $textarea.after($container).on('keydown', dropdown.navigate.bind(dropdown)).on('click keyup', function (e) {
        var _this = this;

        // Up, down, enter, tab, escape, left, right.
        if ([9, 13, 27, 40, 38, 37, 39].indexOf(e.which) !== -1) return;

        var cursor = this.selectionStart;

        if (this.selectionEnd - cursor > 0) return;

        // Search backwards from the cursor for an '~' symbol, without any
        // intervening whitespace. If we find one, we will want to show the
        // autocomplete dropdown!
        var value = this.value;
        mentionStart = 0;
        for (var i = cursor - 1; i >= 0; i--) {
          var character = value.substr(i, 1);
          if (/\s/.test(character)) break;
          if (character === '~') {
            mentionStart = i + 1;
            break;
          }
        }

        dropdown.hide();
        dropdown.active = false;

        if (mentionStart) {
          (function () {
            typed = value.substring(mentionStart, cursor).toLowerCase();

            var makeSuggestion = function makeSuggestion(discussion, user, replacement, content) {
              var className = arguments.length <= 4 || arguments[4] === undefined ? '' : arguments[4];

              if (typed) {
                content = highlight(content, typed);
              }

              return m(
                'button',
                { className: 'PostPreview ' + className,
                  onclick: function () {
                    return applySuggestion(replacement);
                  },
                  onmouseenter: function () {
                    dropdown.setIndex($(this).parent().index());
                  } },
                m(
                  'span',
                  { className: 'PostPreview-content' },
                  avatar(user),
                  content
                )
              );
            };

            var buildSuggestions = function buildSuggestions() {
              var suggestions = [];
              var composerPost = composer.props.post;
              var discuss = composerPost && composerPost.discussion() || composer.props.discussion;
              var discussionId = discuss.id();
              if (typed) {
                app.store.all('discussions').forEach(function (discussion) {
                  var typedLower = typed.toLowerCase();
                  if (discussion.title().toLowerCase().indexOf(typedLower) === -1 || discussion.id() == discussionId) return;
                  var slug = getSlug(discussion.title(), {
                    lang: 'vn'
                  });
                  if (discussion.data.relationships.startUser) {
                    var userId = discussion.data.relationships.startUser.data.id;
                    var user = app.store.getById("users", userId);
                    suggestions.push(makeSuggestion(discussion, user, '[URL=/d/' + discussion.id() + '-' + userId + ']' + discussion.title() + '[/URL]', discussion.title(), 'MentionsDropdown-user'));
                  }
                });
              }

              if (suggestions.length) {
                dropdown.props.items = suggestions;
                m.render($container[0], dropdown.render());

                dropdown.show();
                var coordinates = getCaretCoordinates(_this, mentionStart);
                var width = dropdown.$().outerWidth();
                var height = dropdown.$().outerHeight();
                var _parent = dropdown.$().offsetParent();
                var left = coordinates.left;
                var _top = coordinates.top + 15;
                if (_top + height > _parent.height()) {
                  _top = coordinates.top - height - 15;
                }
                if (left + width > _parent.width()) {
                  left = _parent.width() - width;
                }
                dropdown.show(left, _top);
              }
            };

            buildSuggestions();

            dropdown.setIndex(0);
            dropdown.$().scrollTop(0);
            dropdown.active = true;

            clearTimeout(searchTimeout);
            if (typed) {
              searchTimeout = setTimeout(function () {
                var typedLower = typed.toLowerCase();
                if (searched.indexOf(typedLower) === -1) {
                  app.store.find('discussions', { q: typed, page: { limit: 10 } }).then(function () {
                    if (dropdown.active) buildSuggestions();
                  });
                  searched.push(typedLower);
                }
              }, 250);
            }
          })();
        }
      });
    });
  }

  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumComponentsComposerBody) {
      ComposerBody = _flarumComponentsComposerBody['default'];
    }, function (_flarumHelpersHighlight) {
      highlight = _flarumHelpersHighlight['default'];
    }, function (_flarumHelpersAvatar) {
      avatar = _flarumHelpersAvatar['default'];
    }, function (_flarumUtilsString) {
      truncate = _flarumUtilsString.truncate;
    }, function (_mentionsBinhComponentsAutocompleteDropdown) {
      AutocompleteDropdown = _mentionsBinhComponentsAutocompleteDropdown['default'];
    }],
    execute: function () {}
  };
});;
System.register('mentions/binh/addMentionedByList', ['flarum/extend', 'flarum/Model', 'flarum/models/Post', 'flarum/components/CommentPost', 'flarum/components/PostPreview', 'flarum/helpers/punctuateSeries', 'flarum/helpers/username', 'flarum/helpers/icon'], function (_export) {
  'use strict';

  var extend, Model, Post, CommentPost, PostPreview, punctuateSeries, username, icon;

  _export('default', addMentionedByList);

  function addMentionedByList() {
    Post.prototype.mentionedBy = Model.hasMany('mentionedBy');

    extend(CommentPost.prototype, 'footerItems', function (items) {
      var _this = this;

      var post = this.props.post;
      var replies = post.mentionedBy();

      if (replies && replies.length) {
        var _ret = (function () {
          // If there is only one reply, and it's adjacent to this post, we don't
          // really need to show the list.
          if (replies.length === 1 && replies[0].number() === post.number() + 1) {
            return {
              v: undefined
            };
          }

          var hidePreview = function hidePreview() {
            _this.$('.Post-mentionedBy-preview').removeClass('in').one('transitionend', function () {
              $(this).hide();
            });
          };

          var config = function config(element, isInitialized) {
            if (isInitialized) return;

            var $this = $(element);
            var timeout = undefined;

            var $preview = $('<ul class="Dropdown-menu Post-mentionedBy-preview fade"/>');
            $this.append($preview);

            $this.children().hover(function () {
              clearTimeout(timeout);
              timeout = setTimeout(function () {
                if (!$preview.hasClass('in') && $preview.is(':visible')) return;

                // When the user hovers their mouse over the list of people who have
                // replied to the post, render a list of reply previews into a
                // popup.
                m.render($preview[0], replies.map(function (reply) {
                  return m(
                    'li',
                    { 'data-number': reply.number() },
                    PostPreview.component({
                      post: reply,
                      onclick: hidePreview
                    })
                  );
                }));
                $preview.show();
                setTimeout(function () {
                  return $preview.off('transitionend').addClass('in');
                });
              }, 500);
            }, function () {
              clearTimeout(timeout);
              timeout = setTimeout(hidePreview, 250);
            });

            // Whenever the user hovers their mouse over a particular name in the
            // list of repliers, highlight the corresponding post in the preview
            // popup.
            $this.find('.Post-mentionedBy-summary a').hover(function () {
              $preview.find('[data-number="' + $(this).data('number') + '"]').addClass('active');
            }, function () {
              $preview.find('[data-number]').removeClass('active');
            });
          };

          var users = [];
          var repliers = replies.sort(function (reply) {
            return reply.user() === app.session.user ? -1 : 0;
          }).filter(function (reply) {
            var user = reply.user();
            if (users.indexOf(user) === -1) {
              users.push(user);
              return true;
            }
          });

          var limit = 4;
          var overLimit = repliers.length > limit;

          // Create a list of unique users who have replied. So even if a user has
          // replied twice, they will only be in this array once.
          var names = repliers.slice(0, overLimit ? limit - 1 : limit).map(function (reply) {
            var user = reply.user();

            return m(
              'a',
              { href: app.route.post(reply),
                config: m.route,
                onclick: hidePreview,
                'data-number': reply.number() },
              app.session.user === user ? app.translator.trans('flarum-mentions.forum.post.you_text') : username(user)
            );
          });

          // If there are more users that we've run out of room to display, add a "x
          // others" name to the end of the list. Clicking on it will display a modal
          // with a full list of names.
          if (overLimit) {
            var count = repliers.length - names.length;

            names.push(app.translator.transChoice('flarum-mentions.forum.post.others_text', count, { count: count }));
          }

          items.add('replies', m(
            'div',
            { className: 'Post-mentionedBy', config: config },
            m(
              'span',
              { className: 'Post-mentionedBy-summary' },
              icon('reply'),
              app.translator.transChoice('flarum-mentions.forum.post.mentioned_by' + (replies[0] === app.session.user ? '_self' : '') + '_text', names.length, {
                count: names.length,
                users: punctuateSeries(names)
              })
            )
          ));
        })();

        if (typeof _ret === 'object') return _ret.v;
      }
    });
  }

  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumModel) {
      Model = _flarumModel['default'];
    }, function (_flarumModelsPost) {
      Post = _flarumModelsPost['default'];
    }, function (_flarumComponentsCommentPost) {
      CommentPost = _flarumComponentsCommentPost['default'];
    }, function (_flarumComponentsPostPreview) {
      PostPreview = _flarumComponentsPostPreview['default'];
    }, function (_flarumHelpersPunctuateSeries) {
      punctuateSeries = _flarumHelpersPunctuateSeries['default'];
    }, function (_flarumHelpersUsername) {
      username = _flarumHelpersUsername['default'];
    }, function (_flarumHelpersIcon) {
      icon = _flarumHelpersIcon['default'];
    }],
    execute: function () {}
  };
});;
System.register('mentions/binh/addPostMentionPreviews', ['flarum/extend', 'flarum/components/CommentPost', 'flarum/components/PostPreview', 'flarum/components/LoadingIndicator'], function (_export) {
  'use strict';

  var extend, CommentPost, PostPreview, LoadingIndicator;

  _export('default', addPostMentionPreviews);

  function addPostMentionPreviews() {
    extend(CommentPost.prototype, 'config', function () {
      var contentHtml = this.props.post.contentHtml();

      if (contentHtml === this.oldPostContentHtml || this.isEditing()) return;

      this.oldPostContentHtml = contentHtml;

      var parentPost = this.props.post;
      var $parentPost = this.$();

      this.$('.UserMention, .PostMention').each(function () {
        m.route.call(this, this, false, {}, { attrs: { href: this.getAttribute('href') } });
      });

      this.$('.PostMention').each(function () {
        var $this = $(this);
        var id = $this.data('id');
        var timeout = undefined;

        // Wrap the mention link in a wrapper element so that we can insert a
        // preview popup as its sibling and relatively position it.
        var $preview = $('<ul class="Dropdown-menu PostMention-preview fade"/>');
        $parentPost.append($preview);

        var getPostElement = function getPostElement() {
          return $('.PostStream-item[data-id="' + id + '"]');
        };

        var showPreview = function showPreview() {
          // When the user hovers their mouse over the mention, look for the
          // post that it's referring to in the stream, and determine if it's
          // in the viewport. If it is, we will "pulsate" it.
          var $post = getPostElement();
          var visible = false;
          if ($post.length) {
            var _top = $post.offset().top;
            var scrollTop = window.pageYOffset;
            if (_top > scrollTop && _top + $post.height() < scrollTop + $(window).height()) {
              $post.addClass('pulsate');
              visible = true;
            }
          }

          // Otherwise, we will show a popup preview of the post. If the post
          // hasn't yet been loaded, we will need to do that.
          if (!visible) {
            (function () {
              // Position the preview so that it appears above the mention.
              // (The offsetParent should be .Post-body.)
              var positionPreview = function positionPreview() {
                $preview.show().css('top', $this.offset().top - $parentPost.offset().top - $preview.outerHeight(true)).css('left', $this.offsetParent().offset().left - $parentPost.offset().left).css('max-width', $this.offsetParent().width());
              };

              var showPost = function showPost(post) {
                var discussion = post.discussion();

                m.render($preview[0], [discussion !== parentPost.discussion() ? m(
                  'li',
                  null,
                  m(
                    'span',
                    { className: 'PostMention-preview-discussion' },
                    discussion.title()
                  )
                ) : '', m(
                  'li',
                  null,
                  PostPreview.component({ post: post })
                )]);
                positionPreview();
              };

              var post = app.store.getById('posts', id);
              if (post && post.discussion()) {
                showPost(post);
              } else {
                m.render($preview[0], LoadingIndicator.component());
                app.store.find('posts', id).then(showPost);
                positionPreview();
              }

              setTimeout(function () {
                return $preview.off('transitionend').addClass('in');
              });
            })();
          }
        };

        var hidePreview = function hidePreview() {
          getPostElement().removeClass('pulsate');
          if ($preview.hasClass('in')) {
            $preview.removeClass('in').one('transitionend', function () {
              return $preview.hide();
            });
          }
        };

        $this.on('touchstart', function (e) {
          return e.preventDefault();
        });

        $this.add($preview).hover(function () {
          clearTimeout(timeout);
          timeout = setTimeout(showPreview, 250);
        }, function () {
          clearTimeout(timeout);
          getPostElement().removeClass('pulsate');
          timeout = setTimeout(hidePreview, 250);
        }).on('touchend', function (e) {
          showPreview();
          e.stopPropagation();
        });

        $(document).on('touchend', hidePreview);
      });
    });
  }

  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumComponentsCommentPost) {
      CommentPost = _flarumComponentsCommentPost['default'];
    }, function (_flarumComponentsPostPreview) {
      PostPreview = _flarumComponentsPostPreview['default'];
    }, function (_flarumComponentsLoadingIndicator) {
      LoadingIndicator = _flarumComponentsLoadingIndicator['default'];
    }],
    execute: function () {}
  };
});;
System.register('mentions/binh/addPostReplyAction', ['flarum/extend', 'flarum/components/Button', 'flarum/components/CommentPost', 'flarum/utils/DiscussionControls'], function (_export) {
  'use strict';

  var extend, Button, CommentPost, DiscussionControls;
  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumComponentsButton) {
      Button = _flarumComponentsButton['default'];
    }, function (_flarumComponentsCommentPost) {
      CommentPost = _flarumComponentsCommentPost['default'];
    }, function (_flarumUtilsDiscussionControls) {
      DiscussionControls = _flarumUtilsDiscussionControls['default'];
    }],
    execute: function () {
      _export('default', function () {
        extend(CommentPost.prototype, 'actionItems', function (items) {
          var post = this.props.post;

          if (post.isHidden() || app.session.user && !post.discussion().canReply()) return;

          function insertMention(component, quote) {
            var user = post.user();
            var mention = '@' + (user ? user.username() : post.number()) + '#' + post.id() + ' ';

            // If the composer is empty, then assume we're starting a new reply.
            // In which case we don't want the user to have to confirm if they
            // close the composer straight away.
            if (!component.content()) {
              component.props.originalContent = mention;
            }

            component.editor.insertAtCursor((component.editor.getSelectionRange()[0] > 0 ? '\n\n' : '') + (quote ? '> ' + mention + quote.trim().replace(/\n/g, '\n> ') + '\n\n' : mention));
          }

          items.add('reply', Button.component({
            className: 'Button Button--link',
            children: app.translator.trans('flarum-mentions.forum.post.reply_link'),
            onclick: function onclick() {
              var quote = window.getSelection().toString();

              var component = app.composer.component;
              if (component && component.props.post && component.props.post.discussion() === post.discussion()) {
                insertMention(component, quote);
              } else {
                DiscussionControls.replyAction.call(post.discussion()).then(function (newComponent) {
                  return insertMention(newComponent, quote);
                });
              }
            }
          }));
        });
      });
    }
  };
});;
System.register('mentions/binh/components/AutocompleteDropdown', ['flarum/Component'], function (_export) {
  'use strict';

  var Component, AutocompleteDropdown;
  return {
    setters: [function (_flarumComponent) {
      Component = _flarumComponent['default'];
    }],
    execute: function () {
      AutocompleteDropdown = (function (_Component) {
        babelHelpers.inherits(AutocompleteDropdown, _Component);

        function AutocompleteDropdown() {
          babelHelpers.classCallCheck(this, AutocompleteDropdown);
          babelHelpers.get(Object.getPrototypeOf(AutocompleteDropdown.prototype), 'constructor', this).apply(this, arguments);
        }

        babelHelpers.createClass(AutocompleteDropdown, [{
          key: 'init',
          value: function init() {
            this.active = false;
            this.index = 0;
            this.keyWasJustPressed = false;
          }
        }, {
          key: 'view',
          value: function view() {
            return m(
              'ul',
              { className: 'Dropdown-menu MentionsDropdown' },
              this.props.items.map(function (item) {
                return m(
                  'li',
                  null,
                  item
                );
              })
            );
          }
        }, {
          key: 'show',
          value: function show(left, top) {
            this.$().show().css({
              left: left + 'px',
              top: top + 'px'
            });
            this.active = true;
          }
        }, {
          key: 'hide',
          value: function hide() {
            this.$().hide();
            this.active = false;
          }
        }, {
          key: 'navigate',
          value: function navigate(e) {
            var _this = this;

            if (!this.active) return;

            switch (e.which) {
              case 40:case 38:
                // Down/Up
                this.keyWasJustPressed = true;
                this.setIndex(this.index + (e.which === 40 ? 1 : -1), true);
                clearTimeout(this.keyWasJustPressedTimeout);
                this.keyWasJustPressedTimeout = setTimeout(function () {
                  return _this.keyWasJustPressed = false;
                }, 500);
                e.preventDefault();
                break;

              case 13:case 9:
                // Enter/Tab
                this.$('li').eq(this.index).find('button').click();
                e.preventDefault();
                break;

              case 27:
                // Escape
                this.hide();
                e.stopPropagation();
                e.preventDefault();
                break;

              default:
              // no default
            }
          }
        }, {
          key: 'setIndex',
          value: function setIndex(index, scrollToItem) {
            if (this.keyWasJustPressed && !scrollToItem) return;

            var $dropdown = this.$();
            var $items = $dropdown.find('li');
            var rangedIndex = index;

            if (rangedIndex < 0) {
              rangedIndex = $items.length - 1;
            } else if (rangedIndex >= $items.length) {
              rangedIndex = 0;
            }

            this.index = rangedIndex;

            var $item = $items.removeClass('active').eq(rangedIndex).addClass('active');

            if (scrollToItem) {
              var dropdownScroll = $dropdown.scrollTop();
              var dropdownTop = $dropdown.offset().top;
              var dropdownBottom = dropdownTop + $dropdown.outerHeight();
              var itemTop = $item.offset().top;
              var itemBottom = itemTop + $item.outerHeight();

              var scrollTop = undefined;
              if (itemTop < dropdownTop) {
                scrollTop = dropdownScroll - dropdownTop + itemTop - parseInt($dropdown.css('padding-top'), 10);
              } else if (itemBottom > dropdownBottom) {
                scrollTop = dropdownScroll - dropdownBottom + itemBottom + parseInt($dropdown.css('padding-bottom'), 10);
              }

              if (typeof scrollTop !== 'undefined') {
                $dropdown.stop(true).animate({ scrollTop: scrollTop }, 100);
              }
            }
          }
        }]);
        return AutocompleteDropdown;
      })(Component);

      _export('default', AutocompleteDropdown);
    }
  };
});;
System.register('mentions/binh/components/PostMentionedNotification', ['flarum/components/Notification', 'flarum/helpers/username', 'flarum/helpers/punctuateSeries'], function (_export) {
  'use strict';

  var Notification, username, punctuateSeries, PostMentionedNotification;
  return {
    setters: [function (_flarumComponentsNotification) {
      Notification = _flarumComponentsNotification['default'];
    }, function (_flarumHelpersUsername) {
      username = _flarumHelpersUsername['default'];
    }, function (_flarumHelpersPunctuateSeries) {
      punctuateSeries = _flarumHelpersPunctuateSeries['default'];
    }],
    execute: function () {
      PostMentionedNotification = (function (_Notification) {
        babelHelpers.inherits(PostMentionedNotification, _Notification);

        function PostMentionedNotification() {
          babelHelpers.classCallCheck(this, PostMentionedNotification);
          babelHelpers.get(Object.getPrototypeOf(PostMentionedNotification.prototype), 'constructor', this).apply(this, arguments);
        }

        babelHelpers.createClass(PostMentionedNotification, [{
          key: 'icon',
          value: function icon() {
            return 'reply';
          }
        }, {
          key: 'href',
          value: function href() {
            var notification = this.props.notification;
            var post = notification.subject();
            var auc = notification.additionalUnreadCount();
            var content = notification.content();

            return app.route.discussion(post.discussion(), auc ? post.number() : content && content.replyNumber);
          }
        }, {
          key: 'content',
          value: function content() {
            var notification = this.props.notification;
            var auc = notification.additionalUnreadCount();
            var user = notification.sender();

            return app.translator.transChoice('flarum-mentions.forum.notifications.post_mentioned_text', auc + 1, {
              user: user,
              username: auc ? punctuateSeries([username(user), app.translator.transChoice('flarum-mentions.forum.notifications.others_text', auc, { count: auc })]) : undefined
            });
          }
        }, {
          key: 'excerpt',
          value: function excerpt() {
            return this.props.notification.subject().contentPlain();
          }
        }]);
        return PostMentionedNotification;
      })(Notification);

      _export('default', PostMentionedNotification);
    }
  };
});;
System.register('mentions/binh/components/UserMentionedNotification', ['flarum/components/Notification'], function (_export) {
  'use strict';

  var Notification, UserMentionedNotification;
  return {
    setters: [function (_flarumComponentsNotification) {
      Notification = _flarumComponentsNotification['default'];
    }],
    execute: function () {
      UserMentionedNotification = (function (_Notification) {
        babelHelpers.inherits(UserMentionedNotification, _Notification);

        function UserMentionedNotification() {
          babelHelpers.classCallCheck(this, UserMentionedNotification);
          babelHelpers.get(Object.getPrototypeOf(UserMentionedNotification.prototype), 'constructor', this).apply(this, arguments);
        }

        babelHelpers.createClass(UserMentionedNotification, [{
          key: 'icon',
          value: function icon() {
            return 'at';
          }
        }, {
          key: 'href',
          value: function href() {
            var post = this.props.notification.subject();

            return app.route.discussion(post.discussion(), post.number());
          }
        }, {
          key: 'content',
          value: function content() {
            var user = this.props.notification.sender();

            return app.translator.trans('flarum-mentions.forum.notifications.user_mentioned_text', { user: user });
          }
        }, {
          key: 'excerpt',
          value: function excerpt() {
            return this.props.notification.subject().contentPlain();
          }
        }]);
        return UserMentionedNotification;
      })(Notification);

      _export('default', UserMentionedNotification);
    }
  };
});;
System.register('mentions/binh/main', ['flarum/extend', 'flarum/app', 'flarum/components/NotificationGrid', 'flarum/utils/string', 'mentions/binh/addPostMentionPreviews', 'mentions/binh/addMentionedByList', 'mentions/binh/addPostReplyAction', 'mentions/binh/addComposerAutocomplete', 'mentions/binh/components/PostMentionedNotification', 'mentions/binh/components/UserMentionedNotification'], function (_export) {
  'use strict';

  var extend, app, NotificationGrid, getPlainContent, addPostMentionPreviews, addMentionedByList, addPostReplyAction, addComposerAutocomplete, PostMentionedNotification, UserMentionedNotification;
  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumApp) {
      app = _flarumApp['default'];
    }, function (_flarumComponentsNotificationGrid) {
      NotificationGrid = _flarumComponentsNotificationGrid['default'];
    }, function (_flarumUtilsString) {
      getPlainContent = _flarumUtilsString.getPlainContent;
    }, function (_mentionsBinhAddPostMentionPreviews) {
      addPostMentionPreviews = _mentionsBinhAddPostMentionPreviews['default'];
    }, function (_mentionsBinhAddMentionedByList) {
      addMentionedByList = _mentionsBinhAddMentionedByList['default'];
    }, function (_mentionsBinhAddPostReplyAction) {
      addPostReplyAction = _mentionsBinhAddPostReplyAction['default'];
    }, function (_mentionsBinhAddComposerAutocomplete) {
      addComposerAutocomplete = _mentionsBinhAddComposerAutocomplete['default'];
    }, function (_mentionsBinhComponentsPostMentionedNotification) {
      PostMentionedNotification = _mentionsBinhComponentsPostMentionedNotification['default'];
    }, function (_mentionsBinhComponentsUserMentionedNotification) {
      UserMentionedNotification = _mentionsBinhComponentsUserMentionedNotification['default'];
    }],
    execute: function () {

      app.initializers.add('mentions-binh', function () {
        // For every mention of a post inside a post's content, set up a hover handler
        // that shows a preview of the mentioned post.
        addPostMentionPreviews();

        // In the footer of each post, show information about who has replied (i.e.
        // who the post has been mentioned by).
        addMentionedByList();

        // Add a 'reply' control to the footer of each post. When clicked, it will
        // open up the composer and add a post mention to its contents.
        //addPostReplyAction();

        // After typing '@' in the composer, show a dropdown suggesting a bunch of
        // posts or users that the user could mention.
        addComposerAutocomplete();

        app.notificationComponents.postMentioned = PostMentionedNotification;
        app.notificationComponents.userMentioned = UserMentionedNotification;

        // Add notification preferences.
        extend(NotificationGrid.prototype, 'notificationTypes', function (items) {
          items.add('postMentioned', {
            name: 'postMentioned',
            icon: 'reply',
            label: app.translator.trans('flarum-mentions.forum.settings.notify_post_mentioned_label')
          });

          items.add('userMentioned', {
            name: 'userMentioned',
            icon: 'at',
            label: app.translator.trans('flarum-mentions.forum.settings.notify_user_mentioned_label')
          });
        });

        getPlainContent.removeSelectors.push('a.PostMention');
      });
    }
  };
});